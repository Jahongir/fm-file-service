#Db settings
export ENV_DB_DRIVER=postgres
export ENV_DB_PG_HOST=localhost
export ENV_DB_PG_PORT=5432
export ENV_DB_PG_DBNAME=dev
export ENV_DB_PG_USER=root
export ENV_DB_PG_PASS=root
export ENV_DB_PG_SSLMODE=disable

#RabbitMQ settings
export ENV_RMQ_URL=amqp://localhost:5672
export ENV_RMQ_NETWORK=tcp

#Redis settings
export ENV_REDIS_ADDR=:6379
export ENV_REDIS_NETWORK=tcp

#Microservice address
export ENV_ADDR=:6666

#Sql directory
export ENV_SQLDIR=sql

#Migration table
export ENV_MIGRATETABLE=issue_migrations

#Routing key to fleet created event
export ENV_ROUTINGKEYS_FLEETCREATE=fleet.fleet.created

#Log level
export ENV_LOGLVL=info
