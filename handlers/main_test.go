package handlers

import (
	"fm-libs/api"
	"fm-libs/config"
	"fm-libs/dbutil"
	"fm-libs/handler"
	exlog "fm-libs/log"
	"fm-libs/util"

	"file/conf"
	"file/db"
	"file/models"

	"log"
	"os"
	"testing"
	"time"
)

var (
	FID = "a5962f42-81b6-43bd-bdf7-a8c97d23614a"
	UID = "7ac34a8c-1d82-4f0b-823c-192fa4dba195"

	schema string

	s = handler.BaseRpcReq{SessId: "1"}
)

func TestMain(m *testing.M) {
	var err error
	conf.ConfDir = "../conf"
	conf.RunMode = "test"

	cfg, err := conf.GetConf()
	if err != nil {
		log.Fatal("get conf error: ", err)
	}

	cfg.UploadDir = "../static/"

	cfg.SqlDir = "../sql"
	cfg.MigrateDir = "../migrations"

	DB, err := db.GetDb()
	if err != nil {
		log.Fatal("get db error: ", err)
	}

	if err := Init(); err != nil {
		log.Fatal("handlers init error: ", err)
	}

	//running migrations
	_, err = dbutil.MigrateUp(DB.DB, "postgres", cfg.MigrateTable, cfg.MigrateDir)
	if err != nil {

		log.Fatal("migrate up error: ", err)
	}

	schema = util.FleetSchema(FID)

	DB.MustExec("CREATE SCHEMA IF NOT EXISTS " + schema)

	fl := models.Fleet{}
	if err := fl.Create(FID); err != nil {
		log.Fatal("fleet create erorr: ", err)
	}

	err = handler.Session.Set("1", config.SessUserIdKey, UID)
	if err != nil {
		log.Fatal("session user_id set error: ", err)
	}
	handler.Session.Set("1", config.SessFleetKey, FID)
	if err != nil {
		log.Fatal("session fleet_id set error: ", err)
	}

	exlog.Enabled = false

	go Listen()

	time.Sleep(time.Second * 2)

	if err = api.InitClient(cfg.Addr); err != nil {
		log.Fatal(err)
	}

	exit_code := m.Run()

	DB.MustExec("DROP SCHEMA " + schema + " CASCADE;")

	os.Exit(exit_code)
}
