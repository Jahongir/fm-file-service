package handlers

import (
	"fm-libs/log"

	"net"
	"net/rpc"
)

func Listen() {
	//Registering RPC Objects
	rpc.Register(new(File))

	//Registering port
	l, err := net.Listen("tcp", cf.Addr)
	if err != nil {
		log.Log("rpc listen", log.ErrorLevel, log.M{
			"error": err.Error(),
		})
	}

	//@IMPORTANT do not use HttpServer or HttpListen for rpc
	//It will not work in docker container

	//Serving Registred RPC Objects
	rpc.Accept(l)
}
