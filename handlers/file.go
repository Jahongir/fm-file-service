package handlers

import (
	"file/conf"
	m "file/models"
	"fm-libs/api"
	"fm-libs/config"
	"fm-libs/handler"
	"fm-libs/log"
	pu "fm-libs/preputils"
	v "fm-libs/verr"

	"fmt"
)

type FileJson struct {
	Id       string `json:"id, omitempty" get:"uuid,nonzero" delete:"uuid,nonzero" restore:"uuid,nonzero"`
	Name     string `json:"name" create:"nonzero"`
	Size     int64  `json:"size"`
	Type     string `json:"type"`
	Entity   string `json:"entity" getby_ent_cat:"nonzero"`
	EntityId string `json:"entity_id" getby_ent_cat:"nonzero,uuid"`
	Link     string `json:"link"`
	Category string `json:"category" getby_ent_cat:"nonzero"`
	File     []byte `json:"file"`

	api.PagingReq
	handler.BaseRpcReq
}

type File struct{}

//Request:
// {
// 	"id": "",
// 	"name": "me.jpg",
// 	"file_size": 13,
// 	"mime_type": "image/png",
// 	"entity": "vehicle",
// 	"entity_id": "3c4eaf52-baee-4b17-b726-0c88c202d24d",
// 	"link": "",
// 	"category": "image",
// 	"file": "/9j/2wCEAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nIC",
// 	"sort": null,
// 	"page": 0,
// 	"per_page": 0,
// 	"sess_id": "1"
// },
//Response:
// {
// 	"data": {
// 		"id": "e3251f23-46ae-4d47-af67-c1d4f8700989",
// 		"name": "me.jpg",
// 		"size": 13,
// 		"type": "image/png",
// 		"entity": "vehicle",
// 		"entity_id": "3c4eaf52-baee-4b17-b726-0c88c202d24d",
// 		"link": "/files/vehicle/image/e3251f23-46ae-4d47-af67-c1d4f8700989.jpg",
// 		"category": "image",
// 		"created_at": null,
// 		"updated_at": null,
// 		"deleted_at": null,
// 		"created_by": "7ac34a8c-1d82-4f0b-823c-192fa4dba195",
// 		"updated_by": null,
// 		"deleted_by": null,
// 		"deleted": false
// 	},
// 	"error_code": 0
// }
func (t *File) Create(jsonData []byte, resp *[]byte) error {
	var req FileJson

	*resp = handler.RpcHandle(
		jsonData,
		&req,
		[]string{config.SessUserIdKey},
		func(p *handler.Params) api.Response {
			var model m.File

			//check if category exists in models.MapCategoryToInt
			if err := model.SetCategory(req.Category); err != nil {
				return api.Response{ErrorCode: api.ErrCodeValidation, Data: map[string]string{
					"Category": err.Error(),
				}}
			}

			//check if entity exists in models.MapEntityToInt
			if err := model.SetEntity(req.Entity); err != nil {
				return api.Response{ErrorCode: api.ErrCodeValidation, Data: map[string]string{
					"Entity": err.Error(),
				}}
			}

			model.Name = req.Name
			model.Size = req.Size
			model.Type = req.Type
			model.EntityId = req.EntityId
			model.CreatedBy = p.SessData[config.SessUserIdKey]

			if resp, ok := v.Validate(req, "create"); !ok {
				return resp
			}

			if err := model.Create(p.Schema, req.Entity, req.Category, req.File); err != nil {
				log.Log("model call", log.ErrorLevel, log.M{
					"error": err.Error(),
					"model": fmt.Sprintf("%#v", model),
				})
				return api.Response{ErrorCode: api.ErrCodeModelCall, ErrorMessage: err.Error()}
			}

			pu.SendJsonEmit(Rmq, conf.ServiceName+".file.created", pu.EventData{
				Data:     model,
				SessData: p.SessData,
			})
			return api.Response{ErrorCode: api.ErrCodeSuccess, Data: model}
		})
	return nil
}

//Request:
// {
// 	"id": "e3251f23-46ae-4d47-af67-c1d4f8700989",
// 	"name": "",
// 	"file_size": 0,
// 	"mime_type": "",
// 	"entity": "",
// 	"entity_id": "",
// 	"link": "",
// 	"category": "",
// 	"file": null,
// 	"sort": null,
// 	"page": 0,
// 	"per_page": 0,
// 	"sess_id": "1"
// },
//Response:
// {
// 	"data": {
// 		"id": "e3251f23-46ae-4d47-af67-c1d4f8700989",
// 		"name": "me.jpg",
// 		"size": 13,
// 		"type": "image/png",
// 		"entity": "vehicle",
// 		"entity_id": "3c4eaf52-baee-4b17-b726-0c88c202d24d",
// 		"link": "/files/vehicle/image/e3251f23-46ae-4d47-af67-c1d4f8700989.jpg",
// 		"category": "image",
// 		"created_at": "2016-01-11T17:36:41.253168+05:00",
// 		"updated_at": "2016-01-11T17:36:41.253168+05:00",
// 		"deleted_at": "2016-01-11T17:36:41.253168+05:00",
// 		"created_by": "7ac34a8c-1d82-4f0b-823c-192fa4dba195",
// 		"updated_by": null,
// 		"deleted_by": null,
// 		"deleted": false
// 	},
// 	"error_code": 0
// }
func (t *File) Get(jsonData []byte, resp *[]byte) error {
	var req FileJson

	*resp = handler.RpcHandle(
		jsonData,
		&req,
		[]string{},
		func(p *handler.Params) api.Response {
			var model m.File

			if resp, ok := v.Validate(req, "get"); !ok {
				return resp
			}

			if err := model.Get(p.Schema, req.Id); err != nil {
				log.Log("validation", log.ErrorLevel, log.M{
					"error": err.Error(),
					"model": fmt.Sprintf("%#v", model),
				})
				return api.Response{ErrorCode: api.ErrCodeModelCall, ErrorMessage: err.Error()}
			}
			return api.Response{ErrorCode: api.ErrCodeSuccess, Data: model}
		})
	return nil
}

//Request:
// {
// 	"id": "",
// 	"name": "",
// 	"file_size": 0,
// 	"mime_type": "",
// 	"entity": "",
// 	"entity_id": "",
// 	"link": "",
// 	"category": "",
// 	"file": null,
// 	"sort": null,
// 	"page": 1,
// 	"per_page": 0,
// 	"sess_id": "1"
// },
//Response:
// {
// 	"data": [
// 		{
// 			"id": "e3251f23-46ae-4d47-af67-c1d4f8700989",
// 			"name": "me.jpg",
// 			"size": 13,
// 			"type": "image/png",
// 			"entity": "vehicle",
// 			"entity_id": "3c4eaf52-baee-4b17-b726-0c88c202d24d",
// 			"link": "/files/vehicle/image/e3251f23-46ae-4d47-af67-c1d4f8700989.jpg",
// 			"category": "image",
// 			"created_at": "2016-01-11T17:36:41.253168+05:00",
// 			"updated_at": "2016-01-11T17:36:41.253168+05:00",
// 			"deleted_at": "2016-01-11T17:36:41.253168+05:00",
// 			"created_by": "7ac34a8c-1d82-4f0b-823c-192fa4dba195",
// 			"updated_by": null,
// 			"deleted_by": null,
// 			"deleted": false
// 		}
// 	],
// 	"error_code": 0,
// 	"paging": {
// 		"page": 1,
// 		"per_page": 0,
// 		"max": 1
// 	}
// }
func (t *File) GetAll(jsonData []byte, resp *[]byte) error {
	var req FileJson

	*resp = handler.RpcHandle(
		jsonData,
		&req,
		[]string{config.SessFleetKey},
		func(p *handler.Params) api.Response {
			var model m.File

			res, pages, err := model.GetAll(p.Schema, req.Sort, req.Page, req.PageSize)

			switch err {
			case m.ErrNotFound:
				return api.Response{ErrorCode: api.ErrCodeNotFound, ErrorMessage: "item not found in database"}
			case m.ErrPageNotFound:
				return api.Response{ErrorCode: api.ErrCodePageNotFound, ErrorMessage: "page not found"}
			case nil:
				return api.Response{
					ErrorCode: api.ErrCodeSuccess,
					Data:      res,
					Paging: api.Paging{
						Page:     req.Page,
						PageSize: req.PageSize,
						Max:      pages,
					},
				}
			default:
				log.Log("model call", log.ErrorLevel, log.M{
					"error": err.Error(),
				})
				return api.Response{ErrorCode: api.ErrCodeModelCall, ErrorMessage: err.Error()}
			}
		})
	return nil
}

//Request:
// {
// 	"id": "e3251f23-46ae-4d47-af67-c1d4f8700989",
// 	"sess_id": "1"
// },
//Response:
// {
// 	"error_code": 0
// }

func (t *File) Delete(jsonData []byte, resp *[]byte) error {
	var req FileJson

	*resp = handler.RpcHandle(
		jsonData,
		&req,
		[]string{config.SessUserIdKey, config.SessFleetKey},
		func(p *handler.Params) api.Response {
			var model m.File
			model.Id = req.Id
			model.DeletedBy.String = p.SessData[config.SessUserIdKey]
			model.DeletedBy.Valid = true

			if resp, ok := v.Validate(req, "delete"); !ok {
				return resp
			}

			if err := model.Delete(p.Schema); err != nil {
				log.Log("model call", log.ErrorLevel, log.M{
					"err":   err.Error(),
					"model": fmt.Sprintf("%#v", model),
				})
				return api.Response{ErrorCode: api.ErrCodeModelCall, ErrorMessage: err.Error()}
			}

			pu.SendJsonEmit(Rmq, conf.ServiceName+".file.deleted", pu.EventData{
				Data:     req,
				SessData: p.SessData,
			})
			return api.Response{ErrorCode: api.ErrCodeSuccess}
		})

	return nil
}

//Request:
// {
// 	"id": "e3251f23-46ae-4d47-af67-c1d4f8700989",
// 	"sess_id": "1"
// },
//Response:
// {
// 	"error_code": 0
// }
func (t *File) Restore(jsonData []byte, resp *[]byte) error {
	var req FileJson

	*resp = handler.RpcHandle(
		jsonData,
		&req,
		[]string{config.SessFleetKey},
		func(p *handler.Params) api.Response {
			var model m.File
			model.Id = req.Id

			if resp, ok := v.Validate(req, "restore"); !ok {
				return resp
			}

			if err := model.Restore(p.Schema); err != nil {
				log.Log("model call", log.ErrorLevel, log.M{
					"err":   err.Error(),
					"model": fmt.Sprintf("%#v", model),
				})
				return api.Response{ErrorCode: api.ErrCodeModelCall, ErrorMessage: err.Error()}
			}

			pu.SendJsonEmit(Rmq, conf.ServiceName+".file.restored", pu.EventData{
				Data:     req,
				SessData: p.SessData,
			})
			return api.Response{ErrorCode: api.ErrCodeSuccess}
		})

	return nil
}

//Request:
// {
// 	"id": "",
// 	"name": "",
// 	"file_size": 0,
// 	"mime_type": "",
// 	"entity": "vehicle",
// 	"entity_id": "",
// 	"link": "",
// 	"category": "image",
// 	"file": null,
// 	"sort": null,
// 	"page": 1,
// 	"per_page": 0,
// 	"sess_id": "1"
// },
//Response:
// {
// 	"data": [
// 		{
// 			"id": "e3251f23-46ae-4d47-af67-c1d4f8700989",
// 			"name": "me.jpg",
// 			"size": 13,
// 			"type": "image/png",
// 			"entity": "vehicle",
// 			"entity_id": "3c4eaf52-baee-4b17-b726-0c88c202d24d",
// 			"link": "/files/vehicle/image/e3251f23-46ae-4d47-af67-c1d4f8700989.jpg",
// 			"category": "image",
// 			"created_at": "2016-01-11T17:36:41.253168+05:00",
// 			"updated_at": "2016-01-11T17:36:41.253168+05:00",
// 			"deleted_at": "2016-01-11T12:36:41.284586+05:00",
// 			"created_by": "7ac34a8c-1d82-4f0b-823c-192fa4dba195",
// 			"updated_by": null,
// 			"deleted_by": "7ac34a8c-1d82-4f0b-823c-192fa4dba195",
// 			"deleted": false
// 		}
// 	],
// 	"error_code": 0,
// 	"paging": {
// 		"page": 1,
// 		"per_page": 0,
// 		"max": 1
// 	}
// }
func (t *File) GetByEntityCategory(jsonData []byte, resp *[]byte) error {
	var req FileJson

	*resp = handler.RpcHandle(
		jsonData,
		&req,
		[]string{config.SessFleetKey},
		func(p *handler.Params) api.Response {
			var model m.File

			if resp, ok := v.Validate(req, "getby_ent_cat"); !ok {
				return resp
			}
			res, pages, err := model.GetByEntityCategory(p.Schema, req.Entity, req.Category, req.EntityId, req.Sort, req.Page, req.PageSize)

			switch err {
			case m.ErrNotFound:
				return api.Response{ErrorCode: api.ErrCodeNotFound, ErrorMessage: "item not found in database"}
			case m.ErrPageNotFound:
				return api.Response{ErrorCode: api.ErrCodePageNotFound, ErrorMessage: "page not found"}
			case nil:
				return api.Response{
					ErrorCode: api.ErrCodeSuccess,
					Data:      res,
					Paging: api.Paging{
						Page:     req.Page,
						PageSize: req.PageSize,
						Max:      pages,
					},
				}
			default:
				log.Log("model call", log.ErrorLevel, log.M{
					"error": err.Error(),
				})
				return api.Response{ErrorCode: api.ErrCodeModelCall, ErrorMessage: err.Error()}
			}
		})
	return nil
}
