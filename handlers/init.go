package handlers

import (
	"github.com/streadway/amqp"
	V "gopkg.in/validator.v2"

	"file/conf"
	"file/logger"
	"file/models"
	"file/rabbit"
	"file/redis"
	"fm-libs/handler"
	"fm-libs/session/redis"
	"fm-libs/verr"

	"fmt"
)

var (
	cf  *conf.AppConf
	Rmq *amqp.Connection
)

type (
	baseReq struct {
		handler.BaseRpcReq
		Id string `json:"id"`
	}
)

const (
	ErrMsgItemsNotFound = "Items not found"
	ErrMsgPageNotFound  = "Page not found"
)

func initConf() error {
	//for not creating config in every call
	if cf != nil {
		return nil
	}

	var err error
	cf, err = conf.GetConf()

	if err != nil {
		fmt.Errorf("get config error: ", err)
	}

	return nil
}

func Init() error {

	if cf != nil {
		return nil
	}

	var err error
	if err := initConf(); err != nil {
		return err
	}

	if err := models.Init(); err != nil {
		return fmt.Errorf("models init error: %s", err)
	}

	rds, err := redis.GetRedis()
	if err != nil {
		return fmt.Errorf("redis get error: %s", err)
	}

	handler.Session = &session.RedisStore{Conn: rds}

	V.SetValidationFunc("uuid", verr.UUIDValid)

	Rmq, err = rabbit.GetRabbit()
	if err != nil {
		return fmt.Errorf("rabbit get error: %s", err)
	}

	if err := logger.Init(); err != nil {
		return fmt.Errorf("logger init error: %s", err)
	}

	return nil
}
