package redis

import (
	"file/conf"
	"fm-libs/redis"

	"fmt"
)

var (
	cf *conf.AppConf
)

func initConf() error {
	var err error
	cf, err = conf.GetConf()
	return err
}

func GetRedis() (*redis.Conn, error) {
	if err := initConf(); err != nil {
		return nil, err
	}

	rd, err := redis.Dial(cf.Redis.Network, cf.Redis.Addr)
	if err != nil {
		return nil, fmt.Errorf("redis dial error: %s network: %s addr: %s", err, cf.Redis.Network, cf.Redis.Addr)
	}

	return rd, nil
}
