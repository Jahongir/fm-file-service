-- +migrate Up
CREATE TABLE IF NOT EXISTS file_files (
	id UUID PRIMARY KEY NOT NULL,
	name TEXT NOT NULL,
	file_size BIGINT NOT NULL,
	mime_type TEXT,
	entity TEXT NOT NULL,
	entity_id TEXT NOT NULL,
	link TEXT UNIQUE NOT NULL,
	category TEXT NOT NULL,

	deleted BOOLEAN DEFAULT FALSE,
	created_at TIMESTAMP WITH TIME ZONE DEFAULT current_timestamp,
	updated_at TIMESTAMP WITH TIME ZONE DEFAULT current_timestamp,
	deleted_at TIMESTAMP WITH TIME ZONE DEFAULT current_timestamp,
	created_by UUID NOT NULL,
	updated_by UUID,
	deleted_by UUID
);

-- +migrate Down
DROP TABLE file_files;