package event

import (
	"fm-libs/log"
	"fm-libs/util"
	"issue/conf"
	"issue/db"

	"os"
	"testing"
)

const (
	FID  = "6fa82bb1-c9a8-4aa6-8a9e-cb4e26f84712"
	UUID = "a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11"

	SERVICE_ROUTE_KEY = "fleet.fleet.created"
)

var (
	schema string
)

func TestMain(m *testing.M) {
	conf.ConfDir = "../conf"

	cfg, err := conf.GetConf()
	if err != nil {
		log.Log("get conf", log.FatalLevel, log.M{
			"error": err.Error(),
		})
	}

	cfg.SqlDir = "../sql"

	if err := Init(); err != nil {
		log.Log("init ", log.FatalLevel, log.M{
			"error": err.Error(),
		})
	}

	schema = util.FleetSchema(FID)

	db, err := db.GetDb()
	if err != nil {
		log.Log("get db ", log.FatalLevel, log.M{
			"error": err.Error(),
		})
	}

	//Create fleet schema
	db.MustExec("CREATE SCHEMA IF NOT EXISTS " + schema)

	exitCode := m.Run()

	//Delete fleet schema
	db.MustExec("DROP SCHEMA IF EXISTS " + schema + " CASCADE;")

	os.Exit(exitCode)
}
