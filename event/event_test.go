package event

import (
	"github.com/streadway/amqp"

	rbt "fm-libs/rabbit"

	"encoding/json"
	"fm-libs/preputils"
	"fmt"
	"testing"
	"time"
)

//Emit function for creating fake fleet.created event
func Emit(key string, data interface{}) error {
	ch, err := rmq.Channel()
	if err != nil {
		return fmt.Errorf("rabbit channel create error: %s", err)
	}

	json, err := json.Marshal(data)
	if err != nil {
		return fmt.Errorf("marshal error: %s", err)
	}

	routing_key := key

	if err = ch.Publish(rbt.ExchangeEvents, routing_key, false, false, amqp.Publishing{
		ContentType:  "application/json",
		Body:         json,
		DeliveryMode: amqp.Transient,
	}); err != nil {
		return fmt.Errorf("rabbit publish error: %s", err)
	}
	return nil
}

func TestListenFleet(t *testing.T) {
	_, errChan := ListenFleetCreate()

	acl_succChan := make(chan string, 10)

	rbt.Watch(rmq, "test-issue.fleet.create", SERVICE_ROUTE_KEY, rbt.ExchangeEvents, func(event amqp.Delivery) {
		err := event.Ack(false)
		if err == nil {
			acl_succChan <- "occured"
		}

	})

	if err := Emit(
		cf.RoutingKeys.FleetCreate,
		preputils.FleetEventData{
			Data: preputils.Fleet{
				Id:        FID,
				CreatedBy: UUID,
			},
		},
	); err != nil {
		t.Error("publish error ", err)
	}

	select {
	case err := <-errChan:
		if err != nil {
			t.Error("Listen error ", err)
		}
	case <-acl_succChan:
	case <-time.After(5 * time.Second):
		t.Error("Event not emitted")
	}

}
