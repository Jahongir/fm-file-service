package event

import (
	"github.com/streadway/amqp"

	"file/conf"
	"file/logger"
	"file/models"
	"file/rabbit"
	"fm-libs/log"
	rbt "fm-libs/rabbit"

	"encoding/json"
	"fm-libs/preputils"
	"fmt"
)

var (
	rmq *amqp.Connection
	cf  *conf.AppConf
)

const (
	FLEET_QUEUE = "file-fleet.fleet.create"
)

func initConf() error {
	//for not creating config in every call
	if cf != nil {
		return nil
	}

	var err error
	cf, err = conf.GetConf()

	if err != nil {
		fmt.Errorf("get config error: %s", err)
	}

	return nil
}

//Init event module
func Init() error {
	var err error

	if err := initConf(); err != nil {
		return err
	}

	err = logger.Init()
	if err != nil {
		return fmt.Errorf("init log error: %s", err)
	}

	rmq, err = rabbit.GetRabbit()
	if err != nil {
		return fmt.Errorf("get rabbit error: %s", err)
	}

	if err := models.Init(); err != nil {
		return fmt.Errorf("models init error: %s", err)
	}

	return nil
}

//Listen for fleet create event and create new schema and insert initial data
//returns fleetId on success and errors on error
func ListenFleetCreate() (<-chan string, <-chan error) {
	errChan := make(chan error, 10)
	succChan := make(chan string, 10)

	go func() {
		for succ := range succChan {
			log.Log("listening success channel", log.InfoLevel, log.M{
				"message": succ,
			})
		}
	}()

	go func() {
		for err := range errChan {
			log.Log("listening error channel", log.ErrorLevel, log.M{
				"message": err.Error(),
			})
		}
	}()

	log.Log("listening rabbit event", log.InfoLevel, log.M{
		"event": cf.RoutingKeys.FleetCreate,
	})

	rbt.Watch(rmq, FLEET_QUEUE, cf.RoutingKeys.FleetCreate, rbt.ExchangeEvents, func(event amqp.Delivery) {
		var eventData preputils.FleetEventData

		log.Log("event occured", log.DebugLevel, log.M{
			"event": cf.RoutingKeys.FleetCreate,
			"json":  string(event.Body),
		})

		if err := json.Unmarshal(event.Body, &eventData); err != nil {
			log.Log("event unmarshal", log.ErrorLevel, log.M{
				"event": cf.RoutingKeys.FleetCreate,
				"error": err.Error(),
			})

			errChan <- err
			return
		}

		log.Log("event unmarshalled", log.DebugLevel, log.M{
			"event": cf.RoutingKeys.FleetCreate,
			"dest":  eventData,
		})

		fleet := eventData.Data

		//create new fleet schema
		fl_model := models.Fleet{}
		err := fl_model.Create(fleet.Id)
		if err != nil {
			log.Log("fleet create", log.ErrorLevel, log.M{
				"event": cf.RoutingKeys.FleetCreate,
				"error": err.Error(),
			})
			errChan <- err
			return
		}

		log.Log("event fleet created", log.DebugLevel, log.M{
			"event": cf.RoutingKeys.FleetCreate,
			"fleet": fleet.Id,
		})

		fl := map[string]interface{}{
			"id":         fleet.Id,
			"created_by": fleet.CreatedBy,
		}

		//sending event acl.fleet.created
		if err := rabbit.Emit("fleet.created", fl); err != nil {
			log.Log("event emit", log.ErrorLevel, log.M{
				"event": cf.RoutingKeys.FleetCreate,
				"error": err.Error(),
			})

			errChan <- err
			return
		}

		//Aknowleding rabbit
		if err := event.Ack(false); err != nil {
			log.Log("event acknowlegde", log.WarnLevel, log.M{
				"event": cf.RoutingKeys.FleetCreate,
				"error": err.Error(),
			})

			errChan <- err
			return
		}

		log.Log("event: fleet.created", log.InfoLevel, log.M{
			"event": cf.RoutingKeys.FleetCreate,
			"fleet": fleet.Id,
		})

		succChan <- fleet.Id
	})

	return succChan, errChan
}
