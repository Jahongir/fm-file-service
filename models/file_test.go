package models

import (
	"io/ioutil"
	"os"
	"testing"
)

var (
	EntityId = "8b56e593-adb3-4d49-9dda-74995c37e10c"
	FileId   string
	UUID     = "e7ed944a-8fea-4272-9e9e-84c09a60dd8f"
)

func TestFileCreate(t *testing.T) {
	fileToBeUploaded := "../globe2.png"
	file, err := os.Open(fileToBeUploaded)
	if err != nil {
		t.Error(err)
	}

	file_bytes, err := ioutil.ReadAll(file)
	if err != nil {
		t.Error(err)
	}

	model := File{}
	model.Name = "some_file.jpg"
	model.Size = 105
	model.Type = "image/jpeg"
	model.Entity = "work"
	model.Category = "image"
	model.EntityId = EntityId
	model.CreatedBy = UUID

	if err := model.Create(schema, "work", "image", file_bytes); err != nil {
		t.Error(err)
	}

	if model.Id == "" {
		t.Error("insert id not generated")
	}

	FileId = model.Id
}

func TestFileGet(t *testing.T) {
	model := File{}
	if err := model.Get(schema, FileId); err != nil {
		t.Errorf("model get error %v", err)
		return
	}
}

func TestFileGetAll(t *testing.T) {
	model := File{}
	dests, pages, err := model.GetAll(schema, []string{""}, 0, 0)
	if err != nil {
		t.Error(err)
	}
	if len(dests) < 1 {
		t.Error("dests length should be grater than 0")
	}

	if pages < 1 {
		t.Error("pages should be grater than 0")
	}

}

func TestFileDelete(t *testing.T) {
	model := File{}
	model.Id = FileId
	model.DeletedBy.String = UID
	model.DeletedBy.Valid = true

	if err := model.Delete(schema); err != nil {
		t.Error(err)
	}
}

func TestFileRestore(t *testing.T) {
	model := File{}
	model.Id = FileId
	if err := model.Restore(schema); err != nil {
		t.Error(err)
	}
}

func TestFileCountByEntityCategory(t *testing.T) {
	model := File{}
	cnt, err := model.CountByEntityCategory(schema, "work", "image", EntityId)
	if err != nil {
		t.Error(err)
	}
	if cnt < 1 {
		t.Error("count should be grater than 0")
	}
}

func TestFileGetByEntityCategory(t *testing.T) {
	model := File{}
	dests, pages, err := model.GetByEntityCategory(schema, "work", "image", EntityId, []string{""}, 0, 0)
	if err != nil {
		t.Error(err)
	}
	if len(dests) < 1 {
		t.Error("dests length should be grater than 0")
		return
	}

	if pages < 1 {
		t.Error("pages should be grater than 0")
		return
	}
}
