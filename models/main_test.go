package models

import (
	"file/conf"
	"file/db"
	"fm-libs/util"

	"fm-libs/dbutil"
	fmlog "fm-libs/log"
	"log"
	"os"
	"testing"
)

var (
	FID = "a30f801e-2ac2-4822-8234-25c545834624"
	UID = "7ac34a8c-1d82-4f0b-823c-192fa4dba195"

	schema string
)

func TestMain(m *testing.M) {
	conf.ConfDir = "../conf"
	conf.RunMode = "test"

	cfg, err := conf.GetConf()
	if err != nil {
		log.Fatal("get conf error: ", err)
	}

	cfg.UploadDir = "../static/"

	DB, err := db.GetDb()
	if err != nil {
		log.Fatal("get db error: ", err)
	}

	cfg.SqlDir = "../sql"
	cfg.MigrateDir = "../migrations"

	if err := Init(); err != nil {
		log.Fatal("models init error: ", err)
	}

	//running migrations
	_, err = dbutil.MigrateUp(DB.DB, "postgres", cfg.MigrateTable, cfg.MigrateDir)
	if err != nil {
		fmlog.Log("migrate up", fmlog.FatalLevel, fmlog.M{"error": err.Error(), "table": cf.MigrateTable})
	}

	schema = util.FleetSchema(FID)

	DB.MustExec("CREATE SCHEMA IF NOT EXISTS " + schema)

	fl := Fleet{}
	if err := fl.Create(FID); err != nil {
		log.Fatal("fleet create erorr: ", err)
	}

	exit_code := m.Run()

	DB.MustExec("DROP SCHEMA " + schema + " CASCADE;")

	os.Exit(exit_code)
}
