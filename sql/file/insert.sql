INSERT INTO {schema}.{table}
    ( 
	id, 
	name,
	file_size,
	mime_type,
	entity,
	entity_id,
	link,
	category,
	created_by 
	) 
VALUES
    ( 
	:id, 
	:name,
	:file_size,
	:mime_type,
	:entity,
	:entity_id,
	:link,
	:category,
	:created_by
	) 
RETURNING *
