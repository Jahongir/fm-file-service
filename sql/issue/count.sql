SELECT count(*)
FROM {schema}.{table} s
INNER JOIN {schema}.{vehicle_table} v ON s.vehicle_id=v.id
INNER JOIN {schema}.{odometer_table} od ON s.odometer_id=od.id
WHERE s.deleted=false