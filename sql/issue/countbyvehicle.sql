SELECT count(*)
FROM {schema}.{table} s
INNER JOIN {schema}.{odometer_table} od ON s.odometer_id=od.id
WHERE s.deleted=false AND s.vehicle_id=$1