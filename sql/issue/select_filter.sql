SELECT
	s.*,

	-- Vehicle Columns
	v.id "v.id",
	v.name "v.name",
	v.year "v.year",
	v.fuel_tank_size "v.fuel_tank_size",
	v.fuel_type "v.fuel_type",
	v.reg_state_id "v.reg_state_id",
	v.color_id "v.color_id",
	v.type_id "v.type_id",
	v.man_id "v.man_id",
	v.model_id "v.model_id",

	v.created_at "v.created_at",
	v.updated_at "v.updated_at",
	v.deleted_at "v.deleted_at",
	v.created_by "v.created_by",
	v.updated_by "v.updated_by",
	v.deleted_by "v.deleted_by",
	v.deleted "v.deleted",

	-- Odometer Columns
	od.id "od.id",
	od.vehicle_id "od.vehicle_id",
	od.value "od.value",
	od.is_void "od.is_void",

	od.created_at "od.created_at",
	od.updated_at "od.updated_at",
	od.deleted_at "od.deleted_at",
	od.created_by "od.created_by",
	od.updated_by "od.updated_by",
	od.deleted_by "od.deleted_by",
	od.deleted "od.deleted"

FROM {schema}.{table} s
INNER JOIN {schema}.{vehicle_table} v ON s.vehicle_id=v.id
INNER JOIN {schema}.{odometer_table} od ON s.odometer_id=od.id
WHERE s.deleted=false {condition}
ORDER BY ?
LIMIT ?
OFFSET ?