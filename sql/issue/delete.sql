UPDATE {schema}.{table}
SET deleted=true, deleted_at=current_timestamp, deleted_by=:deleted_by
WHERE id=:id
