package rabbit

import (
	"file/conf"

	"testing"
)

func init() {
	conf.ConfDir = "../conf"
}

func TestGetRabbit(t *testing.T) {
	if _, err := GetRabbit(); err != nil {
		t.Error("GetRabbit() error: ", err)
	}
}

func TestEmit(t *testing.T) {
	var data struct {
		Id   string
		Name string
	}

	data.Id = "694b01f1-7d86-4580-86cf-1612004b7f80"
	data.Name = "Arnold Shvarts Neger"

	if err := Emit("test.created", data); err != nil {
		t.Error("emit error: ", err)
	}
}
